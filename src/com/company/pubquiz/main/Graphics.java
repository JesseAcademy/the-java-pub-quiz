package com.company.pubquiz.main;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class Graphics {

    private Model model;
    private GraphicsContext gc;

    //Images + Info auf in welchen Methoden/Screens diese zu finden sind
    private Image imageFass = new Image("com/company/pubquiz/resources/images/Fass1.png"); //Alle Screens
    private Image imgLeprMitte = new Image("com/company/pubquiz/resources/images/leprMittegr.png");//Alle Screens - Ausnahme Start- u. Endscreen
    private Image imgLeprStart = new Image("com/company/pubquiz/resources/images/LeprStart.png"); //Start-Screen
    private Image imgKuh = new Image("com/company/pubquiz/resources/images/cow.png"); //1.Frage - Q1/A1
    private Image imgDrache = new Image("com/company/pubquiz/resources/images/chinaLion1.png"); //2.Frage - Q2/A2
    private Image imgErde = new Image("com/company/pubquiz/resources/images/erde1.png"); //3.Frage - Q3/A3
    private Image imgHai = new Image("com/company/pubquiz/resources/images/hai1.png"); //4.Frage - Q4/A4
    private Image imgOzean = new Image("com/company/pubquiz/resources/images/ozean.jpg"); //5.Frage - Q5/A5
    private Image imgKids = new Image("com/company/pubquiz/resources/images/kids Kopie.png"); //6.Frage - Q6/A6 / Bild-Rätsel ohne Lösung
    private Image imgKidsAdults = new Image("com/company/pubquiz/resources/images/GruppenBild - Kopie.png"); // A6 Bild-Rätsel mit Lösung
    private Image imgGuiness = new Image("com/company/pubquiz/resources/images/guiness.png"); //6. Antwort - A6
    private Image imgJava = new Image("com/company/pubquiz/resources/images/logo_2018.png"); //7.Frage - Q7/A7
    private Image imgLeprEnde = new Image("com/company/pubquiz/resources/images/dabbingLepre1.png"); //Endscreen
    private Image imgFirework = new Image("com/company/pubquiz/resources/images/firework.png"); //Endscreen

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }


    //Start-Screen - Navigation: Auf den Leprechaun links unten klicken
    public void draw() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(300, 100, 620, 150, 40, 40);
        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 40));
        gc.fillText("Willkommen zum Java-Pub-Quiz ", 340, 183);

        gc.drawImage(imgLeprStart, 80, 450); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.WHITE); //Sprechblase
        gc.fillOval(350, 350, 400, 270);
        gc.setFill(Color.WHITE); //Gedankenblase 1
        gc.fillOval(270, 580, 20, 20);
        gc.setFill(Color.WHITE); //Gedankenblase 2
        gc.fillOval(300, 540, 40, 40);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 32));
        gc.fillText("... und ich bin Larry,\n der Quiz-Master", 420, 470);
    }


    //Screen mit den Spielregeln - Navigation: Auf den roten Go-Knopf oben-mitte drücken
    public void drawRules() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.setFill(Color.RED); //Startbutton
        gc.fillOval(483, 120, 80, 80);
        gc.setFill(Color.GOLD);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 30));
        gc.fillText("GO\n", 500, 170);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.WHITE); //Gedankenblase 1
        gc.fillOval(245, 560, 20, 20);
        gc.setFill(Color.WHITE); //Gedankenblase 2
        gc.fillOval(280, 515, 40, 40);
        gc.setFill(Color.WHITE); //Gedankenblase 3
        gc.fillOval(335, 465, 60, 60);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 40));
        gc.fillText("Die Regeln:\n", 235, 165);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 36));

        gc.fillText("\n" +
                        "1. Bildet Teams\n" +
                        "2. Notiert die Antwort auf einen Zettel\n" +
                        "3. Handy oder Laptop sind natürlich Tabu ;)\n" +
                        "4. Die richtige Antwort bzw. Schätzung gewinnt!\n"
                , 225, 175);
    }


    //1. Frage - Navigation: DIGIT1
    public void drawQ1() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass
        gc.drawImage(imgKuh, 750, 500);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("1. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wieviele Mägen hat eine Kuh? \n", 240, 225);
    }


    //1. Antwort - Navigation: F1
    public void drawA1() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass
        gc.drawImage(imgKuh, 750, 500);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("1. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wieviele Mägen hat eine Kuh? \n", 240, 225);

        gc.setFill(Color.RED); //Antwort wird noch definiert
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 30));
        gc.fillText("Eine Kuh hat vier Mägen: Pansen, Netzmagen,\nBlättermagen und den Labmagen!", 240, 300);
    }


    //2. Frage - Navigation: DIGIT2
    public void drawQ2() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("2. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wieviele Zeitzonen gibt es offiziell\n in China?", 240, 225);

        gc.drawImage(imgDrache, 860, 150);
    }


    //2. Antwort - Navigation: F2
    public void drawA2() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("2. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wieviele Zeitzonen gibt es offiziell\n in China?", 240, 225);

        gc.setFill(Color.RED); //Antwort wird noch definiert
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 30));
        gc.fillText("Die geographische Ausdehnung Chinas umfasst\neigentlich fünf Zeitzonen.\n" +
                "Offiziell wird jedoch nur eine Zeitzone verwendet! \n", 240, 310);

        gc.drawImage(imgDrache, 860, 150);
    }


    //3. Frage - Navigation: DIGIT3
    public void drawQ3() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("3. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wieviele Staaten und Länder gibt es? \n", 240, 225);

        gc.drawImage(imgErde, 480, 302);
    }


    //3. Antwort - Navigation: F3
    public void drawA3() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("3. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wieviele Staaten und Länder gibt es? \n", 240, 225);

        gc.setFill(Color.RED); //Antwort wird noch definiert
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 35));
        gc.fillText("Im Jahr 2018 gibt es offizielle 196 Staaten und Länder. \n", 240, 300);

        gc.drawImage(imgErde, 480, 302);
    }


    //4. Frage - Navigation: DIGIT4
    public void drawQ4() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass


        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("4. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wer hat die meisten Zähne?\nEin Moskito, ein Hai, eine Schnecke oder \nein Gürteltier?", 240, 225);

        gc.drawImage(imgHai, 460, 420);
    }


    //4. Antwort - Navigation: F4
    public void drawA4() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("4. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Wer hat die meisten Zähne:\nEin Moskito, ein Hai, eine Schnecke oder \nein Gürteltier?", 240, 225);

        gc.setFill(Color.RED);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 35));
        gc.fillText("Es ist die Schnecke, welche ca. 25.000 Zähne auf\nder Zunge besitzt!", 240, 360);

        gc.drawImage(imgHai, 460, 420);
    }


    //5. Frage - Navigation: DIGIT5
    public void drawQ5() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgOzean, 238, 330);
        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("5. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Ich habe einen Ozean, aber kein Wasser\n - wer bin ich? ", 240, 225);
    }


    //5. Antwort - Navigation: F5
    public void drawA5() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgOzean, 238, 330);
        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass


        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("5. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Ich habe einen Ozean, aber kein Wasser\n - wer bin ich? ", 240, 225);

        gc.setFill(Color.RED); //Antwort wird noch definiert
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 35));
        gc.fillText("Eine Weltkarte! \n", 240, 320);
    }


    //6. Frage - Navigation: DIGIT6
    public void drawQ6() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 430, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass


        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("6. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Welchen Promi als Kind erkennst du? ", 240, 225);
        gc.drawImage(imgKids, 360, 230);
    }


    //6. Antwort - Navigation: F6
    public void drawA6() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 430, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass
        gc.drawImage(imgGuiness, 1082, 468); //Guiness um zum EndFenster zu kommen

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("6. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Welchen Promi als Kind erkennst du? ", 240, 225);
        gc.drawImage(imgKidsAdults, 360, 230);
    }


    //7. Frage - Navigation: DIGIT7
    public void drawQ7() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 430, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass


        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("7. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Eine Maschine, welche Kaffee in\nCode verwandelt? ", 240, 225);
        gc.drawImage(imgJava, 570, 380, 100, 100);
    }


    //7. Antwort - Navigation: F7
    public void drawA7() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 430, 40, 40);

        gc.drawImage(imgLeprMitte, 100, 530); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass


        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("7. Frage: \t\t\t", 240, 160);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Eine Maschine, welche Kaffee in\nCode verwandelt? ", 240, 225);
        gc.drawImage(imgJava, 570, 380, 100, 100);

        gc.setFill(Color.RED); //Antwort wird noch definiert
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 35));
        gc.fillText("Ein Programmierer!\n", 240, 330);
    }


    //End-Screen - Navigation: DIGIT8
    // Mittels Klick auf dem Fass erfolgt der Restart zum Start-Screen
    public void drawEnd() {
        gc.setFill(Color.LIMEGREEN);
        gc.fillRect(0, 0, 1200, 750);

        gc.setFill(Color.GOLD);
        gc.fillRoundRect(180, 80, 860, 360, 40, 40);
        gc.setFill(Color.WHITE);
        gc.fillRoundRect(200, 100, 820, 320, 40, 40);

        gc.drawImage(imgLeprEnde, 100, 260); //Leprechaun
        gc.drawImage(imageFass, 1000, 515); //Fass


        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 50));
        gc.fillText("Glückwunsch !!!", 345, 240);

        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.ITALIC, 40));
        gc.fillText("Ihr seid wahre \"Academycer\" \n", 345, 290);

        gc.drawImage(imgFirework, 825, 10);
    }

}




