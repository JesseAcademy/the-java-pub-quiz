package com.company.pubquiz.main;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;

public class InputHandler{
    private Model model;
    private Graphics graphics;

    //Songs, Irish Pub Music, welche mit den Nummernpad-Tasten 1,2,3 abgespielt werden können
    private String entrySong = new File("C:\\Users\\Jesse\\Desktop\\Academy\\Dachwerk\\Woche 3\\CanvasProjekt2\\src\\entrySong.mp3").toURI().toString();
    private String screamSong = new File("C:\\Users\\Jesse\\Desktop\\Academy\\Dachwerk\\Woche 3\\CanvasProjekt2\\src\\screamSong.mp3").toURI().toString();
    private String endSong = new File("C:\\Users\\Jesse\\Desktop\\Academy\\Dachwerk\\Woche 3\\CanvasProjekt2\\src\\endSong.mp3").toURI().toString();
    MediaPlayer mediaPlayer;
    Timer timer = new Timer(model, graphics);


    public InputHandler(Graphics graphics) {
        this.graphics = graphics;
    }


    public void onClick(MouseEvent event) {

        if (event.getX() >= 1000 && event.getX() <= (1000+213) && event.getY() > 515 && event.getY() <= (515+236)) {
            graphics.draw();  //Bierfass (rechts unten) geht immer zurück auf den Start-Screen
        }
        if (event.getX() >= 80 && event.getX() <= (80 + 266) && event.getY() > 450 && event.getY() <= (450 + 300)) {
            graphics.drawRules(); //Leprachun (links unten) führt vom Start-Screen zu den Regeln-Screen
        }
        if (event.getX()>= 483 && event.getX() <= (483+80) && event.getY() >= 120 && event.getY()<= (120+80)){
            graphics.drawQ1(); //Startbutton (rot) führt vom Regel-Screen zur 1. Frage (Q1)
        }
    }

    //KeyEvents zum Wechseln der Screen-Methoden für die Fragen und Antworten
    public void onKeyReleased(KeyEvent event) throws Exception{

        if (event.getCode() == KeyCode.F1){
            graphics.drawA1();
        }
        if(event.getCode() == KeyCode.DIGIT2){
            graphics.drawQ2();
        }
        if(event.getCode() == KeyCode.F2){
            graphics.drawA2();
        }
        if(event.getCode() == KeyCode.DIGIT3){
            graphics.drawQ3();
        }
        if(event.getCode() == KeyCode.F3){
            graphics.drawA3();
        }
        if(event.getCode() == KeyCode.DIGIT4){
            graphics.drawQ4();
        }
        if(event.getCode() == KeyCode.F4){
            graphics.drawA4();
        }
        if(event.getCode() == KeyCode.DIGIT5){
            graphics.drawQ5();
        }
        if(event.getCode() == KeyCode.F5){
            graphics.drawA5();
        }
        if(event.getCode() == KeyCode.DIGIT6){
            graphics.drawQ6();
        }
        if(event.getCode() == KeyCode.F6){
            graphics.drawA6();
        }
        if( event.getCode() == KeyCode.DIGIT7){
            graphics.drawQ7();
        }
        if( event.getCode() == KeyCode.F7){
            graphics.drawA7();
        }
        if(event.getCode() == KeyCode.DIGIT8){ //Zum End-Screen
            graphics.drawEnd();
        }
        //Zum Abspielen der drei Songs
        if(event.getCode() == KeyCode.NUMPAD1){
            entrySong();
        }
        if(event.getCode() == KeyCode.NUMPAD2){
            screamSong();
        }
        if(event.getCode() == KeyCode.NUMPAD3){
            endSong();
        }
    }

    //Mediaplayer zum Abspielen der Song-Files
    public void entrySong(){
        MediaPlayer playerEntry = new MediaPlayer(new Media(entrySong));
        playerEntry.play();
    }
    public void screamSong(){
        MediaPlayer playerScream = new MediaPlayer(new Media(screamSong));
        playerScream.play();
    }
    public void endSong() {
        MediaPlayer endPlayer = new MediaPlayer( new Media(endSong));
        endPlayer.play();
    }
}





