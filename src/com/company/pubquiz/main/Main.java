package com.company.pubquiz.main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Main extends Application {

    public static final double WIDTH = 1200;
    public static final double HEIGHT = 750;

    private Timer timer;

    @Override
    public void start(Stage primaryStage)  {

        Canvas canvas = new Canvas(WIDTH, HEIGHT);

        Group group = new Group();
        group.getChildren().addAll(canvas);

        Scene scene = new Scene(group);
        primaryStage.setScene(scene);
        primaryStage.show();


        Model model = new Model();
        Graphics graphics = new Graphics(model, canvas.getGraphicsContext2D());
        timer = new Timer(model, graphics);//Ab hier ist die Verbindung abgeschloßen


        InputHandler inputHandler = new InputHandler(graphics);

        scene.setOnKeyReleased(new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent event){

                try {
                    inputHandler.onKeyReleased(event);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                timer.stop();
            }
        });
        canvas.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                inputHandler.onClick(event);
                timer.stop();
            }
        });


        timer.start();
    }

//Zum Stopp des Timers, damit mittels der Click und Tastenbefehle die Screens gewechselt werden kann
    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }
}

