package com.company.pubquiz.main;

//Wenig Input, da die Interaktion für ein Pub-Quiz hauptsächlich innerhalb der wechselnden Screens, gezeichnet in den Graphics, erfolgt.
public class Model {

    private int counter = 0;

    public int getCounter() {
            return counter;
    }

    public void update(long deltaMills){

        counter += deltaMills;
    }

    public void resetCounter() {
        counter = 0;
    }

}
